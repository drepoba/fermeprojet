# Utilisez l'image de base OpenJDK pour Java 11
FROM openjdk:17-alpine

# Copiez le fichier JAR de l'application Spring Boot dans le conteneur
COPY target/demo1-0.0.1-SNAPSHOT.jar /ferme-agricole.jar

# Exécutez l'application Spring Boot lorsque le conteneur démarre
CMD ["java", "-jar", "/ferme-agricole.jar"]