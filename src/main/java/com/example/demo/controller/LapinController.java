package com.example.demo.controller;

import com.example.demo.entity.Lapin;
import com.example.demo.service.LapinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/lapin")
@CrossOrigin("http://localhost:4200")
public class LapinController {

	private final LapinService lapinService;

	@Autowired
	public LapinController(LapinService lapinService) {
		this.lapinService = lapinService;
	}

	@GetMapping("/list")
	public List<Lapin> getAllLapins() {
		return lapinService.getAllLapins();
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Lapin> getLapinById(@PathVariable String id) {
		Optional<Lapin> lapin = lapinService.getLapinById(id);
		return lapin.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping("/save")
	public Lapin saveLapin(@RequestBody Lapin lapin) {
		return lapinService.saveLapin(lapin);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteLapin(@PathVariable String id) {
		lapinService.deleteLapin(id);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/update/{id}")
	public  ResponseEntity<Lapin> updateLapin(@PathVariable String id, @RequestBody Lapin lapin){
      return lapinService.updateLapin(id,lapin);
	}
}
