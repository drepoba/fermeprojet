package com.example.demo.lapinexception;


public class LapinNotFoundException extends RuntimeException{
    public LapinNotFoundException(String id){
        super("Lapin with ID " + id + " not found");
    }
}
