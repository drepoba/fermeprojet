package com.example.demo.repository;

import com.example.demo.entity.Lapin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LapinRepository extends MongoRepository<Lapin, String> {
}
