package com.example.demo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileService {
	@Value("${pathFile}")
	private String UPLOAD_DIR;

	public void uploadFile(MultipartFile file) throws IOException {
		String fileName = file.getOriginalFilename();
		Path filePath = Paths.get(UPLOAD_DIR + fileName);
		Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
	}
}
