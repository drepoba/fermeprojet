package com.example.demo.service;

import com.example.demo.entity.Lapin;
import com.example.demo.lapinexception.LapinNotFoundException;
import com.example.demo.repository.LapinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class LapinService {
	private final LapinRepository lapinRepository;

	@Autowired
	public LapinService(LapinRepository lapinRepository) {
		this.lapinRepository = lapinRepository;
	}

	public List<Lapin> getAllLapins() {
		return lapinRepository.findAll();
	}

	public Optional<Lapin> getLapinById(String id) {
		return lapinRepository.findById(id);
	}

	public Lapin saveLapin(Lapin lapin) {
		LocalDateTime dateHeureDuJour = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		lapin.setDateAjout(dateHeureDuJour.format(formatter));
		lapin.setStatut(true);
		return lapinRepository.save(lapin);
	}

	public ResponseEntity<Lapin> updateLapin(@PathVariable String id, @RequestBody Lapin lapinUpdate) {
		try {
			Lapin updatedLapin = lapinRepository.findById(id)
					.orElseThrow(() -> new LapinNotFoundException(id)); // Lever une exception si le lapin n'est pas trouvé

			updatedLapin.setNom(lapinUpdate.getNom());
			updatedLapin.setSexe(lapinUpdate.getSexe());
			updatedLapin.setFileName(lapinUpdate.getFileName());
            updatedLapin.setStatut(true);
			return ResponseEntity.ok(lapinRepository.save(updatedLapin)); // Renvoyer une réponse OK
		} catch (NumberFormatException e) {
			return ResponseEntity.badRequest().build(); // Renvoyer une réponse Bad Request
		}
	}

	public void deleteLapin(String id) {
		lapinRepository.deleteById(id);
	}

}
