package com.example.demo.service;

import com.example.demo.entity.Lapin;
import com.example.demo.repository.LapinRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class LapinServiceTest {
	@Mock
	private LapinRepository lapinRepository;

	@InjectMocks
	private LapinService lapinService;

	@BeforeEach
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAllLapins() {
		Lapin lapin1 = new Lapin("1","Lapin1", "Mâle", LocalDate.now().toString(),false,"name1");
		Lapin lapin2 = new Lapin("2","Lapin2", "Femelle", LocalDate.now().toString(),false,"name2");

		when(lapinRepository.findAll()).thenReturn(Arrays.asList(lapin1, lapin2));

		List<Lapin> lapins = lapinService.getAllLapins();
		Assertions.assertEquals(2, lapins.size());
		Assertions.assertEquals("Lapin1", lapins.get(0).getNom());
		Assertions.assertEquals("Femelle", lapins.get(1).getSexe());
	}

	@Test
	public void testSaveLapin() {
		// Create a new Lapin object
		Lapin lapin = new Lapin("3","Lapin1", "Mâle", LocalDate.now().toString(),false,"name3");

		// Mocking the behavior of the lapinRepository.save() method
		when(lapinRepository.save(lapin)).thenReturn(lapin);

		// Calling the method under test
		Lapin savedLapin = lapinService.saveLapin(lapin);

		// Assertions
		Assertions.assertEquals("Lapin1", savedLapin.getNom());
		Assertions.assertEquals("Mâle", savedLapin.getSexe());

		// Verify that lapinRepository.save() was called exactly once during the test
		verify(lapinRepository, times(1)).save(lapin);
	}

}
